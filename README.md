# GRIDS_eID

This is the repository for Data Consumers and Data Provider wishing to use the GRIDS_EID project to perform EID KYC with embedded KYB.

The website for this project is [https://grids-cef.eu/] - Please refer to this website for a wide range of information.

This repository is referred to as an API e-Shop in documentation you may see.

The intention of this repository is to provide a single location where developers and users of the GRIDS system can obtain the knowledge and software necessary to interface with the GRIDS platform, either as a Data Consumer (DC) and/or a Data Provider (DP).

The GRIDS project makes use of the OpenID for Identity Assurance specification which can be found [here](https://openid.net/wg/ekyc-ida/) Please note, there is a slight difference in terminology between the OpenID specification and the specification for GRIDS. OpenID use the term 'Relying Party' which is referred to in GRIDS as the 'Data Consumer' - each are referring to the actual business user of the service (i.e. the entity receiving the data)

## What is GRIDS?

GRIDS is an EU funded project designed to facilitate the (in particular) cross-border acceptance of e-identification and remote know-your-customer (KYC) processes where a natural person is claiming to have representation rights for a company. This is required for, e.g. banks, where a natural person claiming to represent a company requires the natural person validated, the company validated and the natural person's relationship to the company validated. 

GRIDS achieves this by integrating directly with the eIDAS authentication platform widely used within Europe, and enhancing that facility through the use of 3rd party Data Providers who provide extra services such as corporate validation to the platform users, whilst providing end to end encryption and trust.

This functionality is needed for several use cases, particularly in regulated industries where Know Your Customer (KYC) and Anti Money Laundering (AML) requirements need to be met.

GRIDS simplifies the online remote onboarding of individual and business customers, whilst allowing business platforms to enlarge their customer base across borders providing access to a secure digital environment where transaction participants are reliably identified and authenticated and where personal data protection principles are enforced and embedded in the interoperability frameworks.



## GRIDS Use Cases

There are a wide range of business use cases that can be built with the library provided. Examples are given in the document [here](https://grids-cef.eu/node/44)
and in the infographics [here](https://grids-cef.eu/content/grids-infographics-use-case-analysis)


## GRIDS Service Description

There is a detailed description of the GRIDS service, its operation, flows and structure available [here](https://grids-cef.eu/node/175)

## GRIDS SDK and Java implementation JAR

The java jar is available on gitlab, [here](https://github.com/ADACOM-SA/grids-sdk)
This link includes code samples, and there is more information available [here](https://grids-cef.eu/node/182)

## Data Provider data points available

The data points available through the GRIDS data provider network is not fixed, and will change over time and with Data Provider capabilities. However, the baseline set of data points (which apply to corporate entities) is given below:

```
legal_name                      = company name
legal_person_identifier         = company registration number
lei                             = company LEI if any/known
vat_registration                = company VAT number is any/known
address                         = company registered office address
tax_reference                   = company tax reference if any / known
sic                             = company SIC or NAICS code if any / known
business_role                   = role of the associated natural person
sub_jurisdiction                = registration area if applicable
trading_status                  
```


